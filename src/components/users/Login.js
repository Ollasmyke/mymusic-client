import React, {Component} from 'react';
import axios from 'axios';
import env from '../../environment';
import { Button, Col, Form, FormGroup, Label, Input} from 'reactstrap';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
        this.usernameChange = this.usernameChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);

        this.loginUser = this.loginUser.bind(this);
    }
    
    usernameChange(event) {
        this.setState({username: event.target.value});
    }
    passwordChange(event) {
        this.setState({password: event.target.value});
    }

    loginUser(event) {
        const user = {
            username: this.state.username,
            password: this.state.password
        };
        const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.login;
        axios.post(route, user);
        event.preventDefault();
        alert('Welcome.. Enjoy My Music App..');
        this.setState({username: '', password: ''})
      };

    render() {
        return (
            <div className="block divcenter col-padding" style={{backgroundColor: '#FFF', maxWidth: 400}}>
                <h4 className="uppercase ls1">Login to your Account</h4>
                <Form onSubmit={this.loginUser}>
                    <FormGroup row>
                        <Label for="loginUsername" sm={4}>Username</Label>
                        <Col sm={8}>
                            <Input type="text" name="loginUsername" id="loginUsername" value={this.state.username} onChange={this.usernameChange} placeholder="Enter User Name" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="loginPassword" sm={4}>Password</Label>
                        <Col sm={8}>
                            <Input type="password" name="loginPassword" id="loginPassword" value={this.state.password} onChange={this.passwordChange} placeholder="Enter Password" />
                        </Col>
                    </FormGroup>
        
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 9 }}>
                            <Button type="submit" value="Submit">Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>                                          
            </div>
        )
    }
}

export default Login;