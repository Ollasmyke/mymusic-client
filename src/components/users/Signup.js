import React, {Component} from 'react';
import axios from 'axios';
import env from '../../environment';
import { Button, Col, Form, FormGroup, Label, Input} from 'reactstrap';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            username: '',
            email: '',
            password: '',
            gender: '',
        };
        this.firstnameChange = this.firstnameChange.bind(this);
        this.lastnameChange = this.lastnameChange.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.genderChange = this.genderChange.bind(this);

        this.createUser = this.createUser.bind(this);
    }

        firstnameChange(event) {
            this.setState({firstname: event.target.value});
        }
        lastnameChange(event) {
            this.setState({lastname: event.target.value});
        }
        usernameChange(event) {
            this.setState({username: event.target.value});
        }
        emailChange(event) {
            this.setState({email: event.target.value});
        }
        passwordChange(event) {
            this.setState({password: event.target.value});
        }
        genderChange(event) {
            this.setState({gender: event.target.value});
        }

        createUser(event) {
            const user = {
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                gender: this.state.gender
            };
            const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.signUp;
            axios.post(route, user);
            event.preventDefault();
            alert('You\'ve successfully created your account. Please Login again, for verification, to Proceed..');
            this.setState({firstname: '', lastname: '', username: '', email: '', password: '', gender: ''})
            // this.toggle();
          };
        

    render() {
        return (
            <div className="block divcenter col-padding" style={{backgroundColor: '#FFF', maxWidth: 800}}>
                <h4 className="uppercase ls1">Create A new Account</h4>
                <Form onSubmit={this.createUser}>
                    <FormGroup row>
                        <Label for="firstname" sm={4}>First Name</Label>
                        <Col sm={8}>
                            <Input type="text" name="firstname" id="firstname" value={this.state.firstname} onChange={this.firstnameChange} placeholder="First Name" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="lastname" sm={4}>Last Name</Label>
                        <Col sm={8}>
                            <Input type="text" name="lastname" id="lastname" value={this.state.lastname} onChange={this.lastnameChange} placeholder="Last Name" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="username" sm={4}>Username</Label>
                        <Col sm={8}>
                            <Input type="text" name="username" id="username" value={this.state.username} onChange={this.usernameChange} placeholder="Username" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="email" sm={4}>E- Mail</Label>
                        <Col sm={8}>
                            <Input type="email" name="email" id="email" value={this.state.email} onChange={this.emailChange} placeholder="E-Mail Address" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="password" sm={4}>Password</Label>
                        <Col sm={8}>
                            <Input type="password" name="password" id="password" value={this.state.password} onChange={this.passwordChange} placeholder="Password" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="gender" sm={4}>Gender</Label>
                        <Col sm={8}>
                            <Input type="select" name="gender" id="gender" value={this.state.gender} onChange={this.genderChange} placeholder="Gender">
                                <option disabled defaultChecked>--select--</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Others</option>
                            </Input>
                        </Col>
                    </FormGroup>
        
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 9 }}>
                            <Button type="submit" value="Submit">Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>                                          
            </div>
        )
    }
}

export default Login;