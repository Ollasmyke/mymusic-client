import React, {Component} from 'react';
import { Modal, ModalHeader, ModalBody, TabContent, TabPane, Nav, NavItem, NavLink} from 'reactstrap';
import classNames from 'classname';
import Login from '../users/Login';
import Signup from '../users/Signup';

import logo from '../../assets/images/logo-dark.png';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            modal: false,   
        };

        this.toggleTabs = this.toggleTabs.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggleTabs(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {

        return (
            <header id="header" className="dark full-header transparent-header no-sticky">
                <div id="header-wrap">
                    <div className="container clearfix">
                        <div id="primary-menu-trigger"><i className="icon-reorder" /></div>
                        <div id="logo">
                            <a href="index.html" className="standard-logo" data-dark-logo={logo}><img src={logo} alt="Canvas Logo" /></a>
                            <a href="index.html" className="retina-logo" data-dark-logo={logo}><img src={logo} alt="Canvas Logo" /></a>
                        </div>
                        <nav id="primary-menu" className="clearfix not-dark">
                            <ul>
                                <li className="active"><a href="#"><div>Home</div></a></li>
                                <li><a href="#" onClick={this.toggle}>
                                        <div>Sign In</div>
                                    </a>
                                </li>
                                <li><a href="#"><div>Sign Out</div></a></li>
                            </ul>
                        </nav>
                        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink className={classNames({active: this.state.activeTab === '1'})} onClick={() => { this.toggleTabs('1'); }}>
                                            Log In
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className={classNames({active: this.state.activeTab === '2'})} onClick={() => { this.toggleTabs('2'); }}>
                                            Sign Up
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </ModalHeader>

                            <ModalBody>
                                <TabContent activeTab={this.state.activeTab}>
                                    <TabPane tabId="1">
                                        <Login />
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <Signup/>
                                    </TabPane>
                                </TabContent>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </header>

        );

    }
}

export default Header;





