import React, {Component} from 'react';
import Modal from '../modals/songsModal/SongsModal';
import PlayListModal from '../modals/playlistModal/PlaylistModal';
import MusicPlayer from '../player/MusicPlayer';
import axios from 'axios';
import lodash from 'lodash';
import env from '../../environment';

import item1 from '../../assets/images/slider/1.jpg';

class Body extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            modalPlaylist: false,
            playlists: [],
        };

        this.toggle = this.toggle.bind(this);
        this.playAll = this.playAll.bind(this);
        this.togglePlaylist = this.togglePlaylist.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    
    togglePlaylist() {
        this.setState({
          modalPlaylist: !this.state.modalPlaylist
        });
      }

      componentDidMount(){
        const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.playlist;
          axios.get(route)
            .then(res => {
                const playlists = res.data;
                this.setState({ playlists });
            });
            
    }

    playAll(){
        const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.songs;
        axios.get(route)
            .then(res => {
                const allSongs = res.data;

                this.setState({ allSongs });
            })

    }

    render() {

        const allSongs = this.state.allSongs;
        console.log(allSongs);

        const playlist = [
        {
          url: 'http://res.cloudinary.com/alick/video/upload/v1502689683/Luis_Fonsi_-_Despacito_ft._Daddy_Yankee_uyvqw9.mp3',
          cover: 'http://res.cloudinary.com/alick/image/upload/v1502689731/Despacito_uvolhp.jpg',
          title: 'Despacito',
          artist: [
            'Luis Fonsi',
            'Daddy Yankee'
          ]
        },
        {
          url: 'http://res.cloudinary.com/alick/video/upload/v1502375674/Bedtime_Stories.mp3',
          cover: 'http://res.cloudinary.com/alick/image/upload/v1502375978/bedtime_stories_bywggz.jpg',
          title: 'Bedtime Stories',
          artist: [
            'Jay Chou'
          ]
        },
        {
          url: 'http://res.cloudinary.com/alick/video/upload/v1502444212/Actor_ud8ccw.mp3',
          cover: 'http://res.cloudinary.com/alick/image/upload/v1502444304/actor_umzdur.jpg',
          title: '演员',
          artist: [
            '薛之谦'
          ]
        },
        {
          url: 'http://res.cloudinary.com/alick/video/upload/v1502444215/Bridge_of_Fate_aaksg1.mp3',
          cover: 'http://res.cloudinary.com/alick/image/upload/v1502444306/Bridge_of_Fate_o36rem.jpg',
          title: 'Bridge of Fate',
          artist: [
            '王力宏',
            '谭维维'
          ]
        },
        {
          url: 'http://res.cloudinary.com/alick/video/upload/v1502444222/Goodbye_byaom5.mp3',
          cover: 'http://res.cloudinary.com/alick/image/upload/v1502444310/Goodbye_hpubmk.jpg',
          title: 'Goodbye',
          artist: [
            'G.E.M.'
          ]
        }
      ]

        return (
            <div >
                <section id="slider" className="slider-element clearfix swiper_wrapper" style={{background: '#131722', marginTop: -80}} data-effect="fade" data-loop="true" data-speed={1000}>
                    <div className="container clearfix" style={{background: `url(${item1}) no-repeat center center`, backgroundSize: 'cover'}}>
                        <div className="slider-caption">
                            <h2 className="font-primary nott" >PLAY ALL</h2>
                            <button className="button button-rounded t400 ls1 clearfix" onClick={this.playAll} style={{marginTop: 15}}><i className="icon-play" />Play Now</button>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}/>
                            <PlayListModal isOpen={this.state.modalPlaylist} togglePlaylist={this.togglePlaylist} className={this.props.className}/>{''}
                        </div>
                    </div>
                </section>
                <section id="content" className="bgcolor2" style={{overflow: 'visible'}}>
                    <div className="content-wrap nopadding">
                        <div className="container clearfix" style={{zIndex: 7}}>
                            <div className="heading-block bottommargin-sm noborder dark" style={{marginTop: '-30px'}}>
                                <h3>My Personal Playlists</h3>
                                <span>Create one now and start Listening to your favourite tracks all in one place..</span> <hr/>
                            </div>
                            <div className="row topmargin-lg clearfix">
                                { this.state.playlists.map(playlist =>
                                <div className="col-lg-6">
                                    <div className="heading-block noborder dark" style={{marginBottom: 15}}>
                                        <h3 >{playlist.name} </h3>
                                        <span >{playlist.description}</span>
                                        <hr />
                                    </div>
                                    <div className="songs-lists-wrap">
                                        <div className="songs-list">
                                            <div className="songs-number">01</div>
                                            {/* <div className="songs-image track-image">
                                                <a href="#" className="track-list" data-track="demos/music/tracks/searchin-lost-european.mp3" data-poster="demos/music/tracks/poster-images/searchin-lost-european.jpg" data-title="Searchin Lost European" data-singer="Lost European">
                                                    <img src="demos/music/tracks/poster-images/searchin-lost-european.jpg" alt="Image 1" /><span><i className="icon-play" /></span></a>
                                            </div> */}
                                            <div className="songs-name track-name"><a href="#">Despacito<br /><span>Luis Fonsi</span></a></div>
                                            <div className="songs-time">04:54</div>
                                            <div className="songs-button"><a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="icon-line-ellipsis" /></a>
                                                <ul className="dropdown-menu">
                                                    <a className="dropdown-item" href="#"><span className="icon-line-plus" /> Add to Queue</a>
                                                    <a className="dropdown-item" href="#"><span className="icon-music" /> Add to Playlist</a>
                                                    <a className="dropdown-item" href="#"><span className="icon-line-cloud-download" /> Download Offline</a>
                                                    <a className="dropdown-item" href="#"><span className="icon-line-heart" /> Love</a>
                                                    <div className="dropdown-divider" />
                                                    <a className="dropdown-item" href="#"><span className="icon-line-share" /> Share</a>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" className="button nobg t300 nott fright noleftmargin" style={{color: '#AAA', padding: '0 16px'}}>See More..</a>
                                </div> )}
                                <div className="w-100 d-block d-md-block d-lg-none topmargin-lg clear" />
                            </div>
                            <div className="clear" />
                        </div>
                    </div>
                </section>
                <MusicPlayer playlist={playlist} autoplay />
            </div>

        );

    }
}

export default Body;





