import React, {Component} from 'react';

class Footer extends Component {



    render() {

        return (
            <footer id="footer" className="noborder dark" style={{backgroundColor: '#111'}}>
                <div id="copyrights" style={{color: '#444'}}>
                    <div className="container clearfix">
                        <div className="col_half">
                            Copyrights © 2017 All Rights Reserved by My Music.
                        </div>
                        <div className="col_half col_last">
                            <div className="fright clearfix">
                                <button tabIndex="0" className="social-icon si-small si-borderless si-facebook">
                                    <i className="icon-facebook" />
                                    <i className="icon-facebook" />
                                </button>
                                <button href="#" className="social-icon si-small si-borderless si-twitter">
                                    <i className="icon-twitter" />
                                    <i className="icon-twitter" />
                                </button>
                                <button href="#" className="social-icon si-small si-borderless si-gplus">
                                    <i className="icon-gplus" />
                                    <i className="icon-gplus" />
                                </button>
                                <button href="#" className="social-icon si-small si-borderless si-github">
                                    <i className="icon-github" />
                                    <i className="icon-github" />
                                </button>
                                <button href="#" className="social-icon si-small si-borderless si-linkedin">
                                    <i className="icon-linkedin" />
                                    <i className="icon-linkedin" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        );

    }
}

export default Footer;





