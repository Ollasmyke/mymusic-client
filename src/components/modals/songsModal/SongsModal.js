import React from 'react';
import axios from 'axios';
import superagent from 'superagent';
import lodash from 'lodash';
import env from '../../../environment';
import { Button, Modal, ModalHeader, ModalBody, Col, Form, FormGroup, Label, Input, FormText} from 'reactstrap';


class SongModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            name: '',
            artist: '',
            playlistId: '',
            track: '',
            playlists: ''
        };

        this.nameChange = this.nameChange.bind(this);
        this.artistChange = this.artistChange.bind(this);
        this.playlistChange = this.playlistChange.bind(this);
        this.trackChange = this.trackChange.bind(this);

        this.submitSong = this.submitSong.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
        modal: !this.state.modal
        });
    }

    componentDidMount(){
        const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.playlist;
        axios.get(route)
            .then(res => {
              const playlists = res.data;
              this.setState({ playlists });
              console.log('playlists:: ', playlists);
        });
            
    }

    nameChange(event) {
        this.setState({name: event.target.value});
    }
    artistChange(event) {
        this.setState({artist: event.target.value});
    }
    playlistChange(event) {
        this.setState({playlistId: event.target.value});
    }
    trackChange(event) {
        this.setState({track: event.target.value});
    }

    submitSong(event) {
        const song = {
          name: this.state.name,
          artist: this.state.artist,
          playlist: this.state.playlist,
          track: this.state.track,
        }
        console.log(song);
        const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.songs;
        axios.post(route, song);
        event.preventDefault();
        // this.toggle();
        this.setState({name: '', artist: '', track: ''})
    }

    render() {
        const options = lodash.map(this.state.playlists, (playlist, pla) => {
            const id = playlist.id;
            // console.log('playlist._id:: ', playlist._id);
            return <option key={playlist._id} value={playlist._id}>{playlist.name}</option>;
            
            
        });
          
        return (
            <div>
                <button onClick={this.toggle} className="button button-rounded t400 ls1 clearfix" style={{marginTop: 15, backgroundColor: '#1f2330', padding: '0 16px'}}><i className="icon-line-plus norightmargin"/>&nbsp;Add New Songs</button>
    
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Add A new Song</ModalHeader>
                    <ModalBody>
    
                        <Form onSubmit={this.submitSong}>
                            <FormGroup row>
                                <Label for="track" sm={2}>Song</Label>
                                <Col sm={10}>
                                    <Input type="file" name="track" value={this.state.track} onChange={this.trackChange} id="track" />
                                    <FormText color="muted">
                                        Select the file to add the song. MP3 file required.
                                    </FormText>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="name" sm={2}>Name</Label>
                                <Col sm={10}>
                                    <Input type="text" name="name" value={this.state.name} onChange={this.nameChange} id="name" placeholder="Name of Song" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="artist" sm={2}>Artist</Label>
                                <Col sm={10}>
                                    <Input type="text" name="artist" value={this.state.artist} onChange={this.artistChange} id="artist" placeholder="Name of Artist" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="playlist" sm={2}>Playlist</Label>
                                <Col sm={10}>
                                    <Input type="select" name="playlist" value={this.state.playlistId} onChange={this.playlistChange} id="playlist">
                                        {options}
                                    </Input>
                                </Col>
                            </FormGroup>
                            
                            <FormGroup check row>
                                <Col sm={{ size: 10, offset: 9 }}>
                                    <Button>Submit</Button>
                                </Col>
                            </FormGroup>
                        </Form>
    
                    </ModalBody>
                </Modal>
            </div>
        );  


    }
}
    
export default SongModal;