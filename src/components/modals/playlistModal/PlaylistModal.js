import React from 'react';
import axios from 'axios';
import env from '../../../environment';
import { Button, Modal, ModalHeader, ModalBody, Col, Form, FormGroup, Label, Input } from 'reactstrap';

class PlaylistModal extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        modalPlaylist: false,
        name: '',
        description: ''
      };
  
      this.nameChange = this.nameChange.bind(this);
      this.descriptionChange = this.descriptionChange.bind(this);

      this.submitPlaylist = this.submitPlaylist.bind(this);
      this.togglePlaylist = this.togglePlaylist.bind(this);
    }
  
    togglePlaylist() {
      this.setState({
        modalPlaylist: !this.state.modalPlaylist
      });
    }

    nameChange(event) {
      this.setState({name: event.target.value});
    }
    descriptionChange(event) {
      this.setState({description: event.target.value});
    }

    submitPlaylist(event) {
      const playlist = {
        name: this.state.name,
        description: this.state.description
      }
      const route = env.API_URL.prefix + env.API_URL.version + env.API_URL.playlist;
      axios.post(route, playlist);
      event.preventDefault();
      alert('Playlist created..');
      this.togglePlaylist();
      this.setState({name: '', description: ''})
    }
  


  
    render() {
        return (
            <div>
                <button onClick={this.togglePlaylist} className="button button-rounded t400 ls1 clearfix" style={{marginTop: 15, backgroundColor: '#1f2330', padding: '0 16px'}}><i className="icon-line-plus norightmargin"/>&nbsp;Create New Playlist</button>

          <Modal isOpen={this.state.modalPlaylist} toggle={this.togglePlaylist} className={this.props.className}>
            <ModalHeader toggle={this.togglePlaylist}>Create A New Playlist</ModalHeader>
            <ModalBody>
            <Form onSubmit={this.submitPlaylist}>
        <FormGroup row>
          <Label for="name" sm={2}>Name</Label>
          <Col sm={10}>
            <Input type="text" name="name" value={this.state.name} onChange={this.nameChange} id="name" placeholder="Name of PLaylist" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="description" sm={3}>Description</Label>
          <Col sm={9}>
            <Input type="textarea" name="description" value={this.state.description} onChange={this.descriptionChange} id="description" placeholder="Add a Little Description" />
          </Col>
        </FormGroup>
        <FormGroup check row>
          <Col sm={{ size: 10, offset: 9 }}>
            <Button type="submit" value="Submit">Submit</Button>
          </Col>
        </FormGroup>
        </Form>
              </ModalBody>
          
          </Modal>
        </div>
      );
    }
  }
  
  export default PlaylistModal;