const env = {
    APIKEY: '',
    PROJECTID:'my-music',
    MONGO_URL: 'mongodb://localhost:27017/mymusic',
    API_URL: {
        prefix: 'http://localhost:8100/',
        version: 'api/v1/',
        get: 'users',
        login: 'users/login',
        signUp: 'users/signup',
        playlist: 'playlist',
        songs: 'songs'
    
    }
}

export default env;