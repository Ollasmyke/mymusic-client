import React, {Component} from 'react';
import Header from './components/header/Header';
import Body from './components/body/Body';
import Footer from './components/footer/Footer';
import Login from './components/users/Login';
import Player from './components/player/Player';
import './App.css';

class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Body/>
                <Footer/>
                {/* <Login/> */}
            </div>
        );
    }
}

export default App;
